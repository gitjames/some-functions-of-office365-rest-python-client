
## how to register an app and get Client_id&Client_secret for SharePoint Site owners.

step 1. site url + /_layouts/15/appregnew.aspx to register an app.  

step 2: site url +/_layouts/15/appinv.aspx to grant permission to the app.  

comment: fill "Permission Request XML" in step 2 with:  

```
<AppPermissionRequests AllowAppOnlyPolicy="true">
   <AppPermissionRequest Scope="http://sharepoint/content/sitecollection/web" Right="FullControl" />
</AppPermissionRequests>

```


##